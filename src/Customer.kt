/**
 * Created by Kenneth on 10/8/2019.
 * for  in UOW-DSA
 */
data class Customer(val arrivalTime: Int, val priSpend: Int, val secSpend: Int) {
    var state = State.STATE_BEGIN
    var timeInStore = 0
}