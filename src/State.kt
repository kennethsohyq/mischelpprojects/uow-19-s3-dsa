/**
 * Created by Kenneth on 10/8/2019.
 * for  in UOW-DSA
 */
object State {
    const val STATE_BEGIN = 0
    const val STATE_PRI_QUEUE = 1
    const val STATE_PRI_SERVE = 2
    const val STATE_SEC_QUEUE = 3
    const val STATE_SEC_SERVE = 4
    const val STATE_FINISH = 5
}