
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

object Main {

    @JvmStatic
    fun main(args: Array<String>) {
        // write your code here
        val f: File = if (args.isEmpty()) {
            println("No arguements. Presuming data.txt as default")
            File("data.txt")
        } else {
            File(args[0])
        }

        if (!f.exists()) {
            println("Cannot find ${f.name}. Exiting")
            return
        }
        readFile(f)

        if (primaryServerCount <= 0 || secondaryServerCount <= 0) {
            println("Not enough servers")
            return
        }

        println("Initializing...")
        // Init some stuff
        for (i in 1..primaryServerCount) {
            primaryServers.add(Server())
        }

        for (i in 1..secondaryServerCount) {
            secondaryServers.add(Server())
        }

        println("Initialization Complete! Starting Simulation...")
        println()
        printStats()
        while (true) {
            // Tick everyone by 1
            timeTaken++
            for (cust in customerArray) {
                if (cust.state != State.STATE_BEGIN && cust.state != State.STATE_FINISH)
                    cust.timeInStore++
            }

            with(unprocessedCustomer.iterator()) {
                forEach {
                    if (it.arrivalTime == timeTaken) {
                        // Time for customer to arrive
                        queuePrimary.add(it)
                        remove()
                        it.state = State.STATE_PRI_QUEUE
                    }
                }
            }

            // Check servers
            for (server in primaryServers) {
                if (server.hasFinishedServing(timeTaken)) {
                    // Move to secondary queue
                    val cust = server.customer ?: throw IllegalStateException("Undetected customer")
                    cust.state = State.STATE_SEC_QUEUE
                    queueSecondary.add(cust)
                    server.finishServing()
                }

                if (server.isFree() && queuePrimary.isNotEmpty()) {
                    server.serveCustomer(queuePrimary.remove(), timeTaken, true)
                }
            }

            for (server in secondaryServers) {
                if (server.hasFinishedServing(timeTaken)) {
                    // Move to done
                    val cust = server.customer ?: throw IllegalStateException("Undetected customer")
                    cust.state = State.STATE_FINISH
                    server.finishServing()
                    customerServed++
                }

                if (server.isFree() && queueSecondary.isNotEmpty()) {
                    server.serveCustomer(queueSecondary.remove(), timeTaken, false)
                }
            }

            if (shouldExit()) {
                printStats()
                break
            }

            // Increment Idle Server Counts
            for (server in primaryServers) {
                if (server.isFree()) server.idle++
            }
            for (server in secondaryServers) {
                if (server.isFree()) server.idle++
            }

            printStats()
        }
        println("Simulation ends")
    }

    private fun shouldExit(): Boolean {
        // Check if we should end the simulation
        if (unprocessedCustomer.isNotEmpty()) return false
        if (queueSecondary.isNotEmpty()) return false
        if (queuePrimary.isNotEmpty()) return false
        for (s in primaryServers) {
            if (!s.isFree()) return false
        }
        for (s in secondaryServers) {
            if (!s.isFree()) return false
        }
        return true
    }

    private fun printStats() {
        println("Time: $timeTaken mins")
        println("Queue Length: Pri: ${queuePrimary.size} | Sec: ${queueSecondary.size}")
        println("Servers:")
        var i = 1
        for (server in primaryServers) {
            println("P$i Status: ${if (server.isFree()) "Free" else "Busy"} | Idle: ${server.idle}")
            i++
        }

        i = 1
        for (server in secondaryServers) {
            println("S$i Status: ${if (server.isFree()) "Free" else "Busy"} | Idle: ${server.idle}")
            i++
        }
        println("Customer Served: $customerServed")
        println()
    }

    private fun readFile(file: File) {
        var firstLine = true
        file.readLines().forEach {
            if (firstLine) {
                val count = it.split(" ")
                primaryServerCount = count[0].toInt()
                secondaryServerCount = count[1].toInt()
                firstLine = false
            } else {
                val data = it.split(" ")
                if (data[0].toInt() == 0 && data[1].toInt() == 0 && data[2].toInt() == 0) {
                    return@forEach // End of simulation
                }
                unprocessedCustomer.add(Customer(data[0].toInt(), data[1].toInt(), data[2].toInt()))
            }
        }
        customerArray.addAll(unprocessedCustomer)
    }

    private var primaryServerCount = 0
    private var secondaryServerCount = 0

    // Counters
    private var timeTaken = 0
    private var customerServed = 0

    private val unprocessedCustomer: ArrayList<Customer> = ArrayList()
    private val customerArray: ArrayList<Customer> = ArrayList()
    private val queuePrimary: Queue<Customer> = LinkedList()
    private val queueSecondary: Queue<Customer> = LinkedList()

    private val primaryServers = ArrayList<Server>()
    private val secondaryServers = ArrayList<Server>()

}
