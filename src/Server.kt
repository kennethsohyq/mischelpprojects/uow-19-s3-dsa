/**
 * Created by Kenneth on 10/8/2019.
 * for  in UOW-DSA
 */
data class Server(var customer: Customer? = null) {
    var timeComplete: Int = -1
    var idle: Int = 0
    var arrivalTime: Int = 0

    fun isFree(): Boolean {
        return customer == null
    }

    fun serveCustomer(customer: Customer, currentTime: Int, isPri: Boolean = true) {
        customer.state = if (isPri) State.STATE_PRI_SERVE else State.STATE_SEC_SERVE
        this.customer = customer
        this.arrivalTime = currentTime
        this.timeComplete = currentTime + (if (isPri) customer.priSpend else customer.secSpend)
    }

    fun hasFinishedServing(currentTime: Int): Boolean {
        return currentTime == timeComplete
    }

    fun finishServing() {
        this.customer = null
        this.timeComplete = -1
    }
}